tool
extends "../tronic.gd"

func calculate():
	var data_a = get_node("DataInA")
	var data_b = get_node("DataInB")
	var data_out = get_node("DataOut")

	data_a = data_a.get_data()
	if(data_a == null):
		data_a = 0

	data_b = data_b.get_data()
	if(data_b == null):
		data_b = 0

	var out = data_a + data_b

	if(data_out != null):
		data_out.store_data(out)

func parse_data(num):
	var out = num

	if(typeof(num) == TYPE_STRING):
		if(num.is_valid_integer()):
			out = int(num)
		elif(num.is_valid_float()):
			out = float(num)

	return out
