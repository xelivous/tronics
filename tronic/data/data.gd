tool
extends "../tronic.gd"

export var data = "" setget store_data, get_data

func store_data(a):
	data = a

func get_data():
	return data