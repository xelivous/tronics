#MADE BY HENRIQUE ALVES
#LICENSE STUFF BLABLABLA
#(MIT License)

extends ReferenceFrame

var _buffer = [] # 0 = Debug; 1 = Text; 2 = Silence; 3 = Break; 4 = Input
var _label # The Label in which the text is going to be displayed

var _output_delay = 0
var _output_delay_limit = 0
var _max_lines
var _max_lines_reached = false
var _buff_beginning = true
var _turbo = true

signal buff_end()
signal tag_buff(tag)

# The text for the output, and its printing velocity (per character)
func buff_text(text, vel = 0.025, tag = "", push_front = false):
	var b = {"buff_text":text, "buff_vel":vel, "buff_tag":tag}
	if (!push_front):
		_buffer.append(b)
	else:
		_buffer.push_front(b)

# Deletes ALL the text on the label
func clear_text():
	_label.set_lines_skipped(0)
	_label.set_text("")

# Clears all buffs in _buffer
func clear_buffer():
	_buffer.clear()

	_output_delay = 0
	_output_delay_limit = 0
	_max_lines_reached = false
	_buff_beginning = true
	_turbo = false

# Reset TIE to its initial 100% cleared state
func reset():
	clear_text()
	clear_buffer()

# Add a new line to the label text
func add_newline():
	_label_print("\n")

# Get current text on Label
func get_text():
	return _label.get_text()

# Print stuff in the maximum velocity and ignore breaks
func set_turbomode(s):
	_turbo = s;

# Changes the color of the text
func set_color(c):
	_label.add_color_override("font_color", c)

# ==============================================
# Reserved methods

func _ready():
	set_process(true)

	_label = Label.new()

	add_child(_label)

	# Setting size of the frame
	_max_lines = floor(get_size().y/_label.get_line_height())
	_label.set_size(Vector2(get_size().x,get_size().y))
	_label.set_autowrap(true)

func _process(delta):
	if(_buffer.size() == 0):
		emit_signal("buff_end")
		return

	var o = _buffer[0] # Calling this var 'o' was one of my biggest mistakes during the development of this code. I'm sorry about this.

	# -- Print Text --

	if(o["buff_tag"] != "" and _buff_beginning == true):
		emit_signal("tag_buff", o["buff_tag"])

	if (_turbo): # In case of turbo, print everything on this buff
		o["buff_vel"] = 0

	if(o["buff_vel"] == 0): # If the velocity is 0, then just print everything
		while(o["buff_text"] != ""): # Not optimal (not really printing everything at the same time); but is the only way to work with line break
			if(o["buff_text"][0] == " " or _buff_beginning):
				_skip_word()
			_label_print(o["buff_text"][0])
			_buff_beginning = false
			o["buff_text"] = o["buff_text"].right(1)
			if(_max_lines_reached == true):
				break

	else: # Else, print each character according to velocity
		_output_delay_limit = o["buff_vel"]
		if(_buff_beginning):
			_output_delay = _output_delay_limit + delta
		else:
			_output_delay += delta
		if(_output_delay > _output_delay_limit):
			if(o["buff_text"][0] == " " or _buff_beginning):
				_skip_word()
			_label_print(o["buff_text"][0])
			_buff_beginning = false
			_output_delay -= _output_delay_limit
			o["buff_text"] = o["buff_text"].right(1)

	# -- Popout Buff --
	if (o["buff_text"] == ""): # This buff finished, so pop it out of the array
		_buffer.pop_front()
		_buff_beginning = true
		_output_delay = 0

# ==============================================
# Private
func _get_last_line():
	var i = _label.get_text().rfind("\n")
	if (i == -1):
		return _label.get_text()
	return _label.get_text().substr(i,_label.get_text().length()-i)

func _has_to_skip_word(word): # what an awful name
	var ret = false
	var n = _label.get_line_count()
	_label.set_text(_label.get_text() + word)
	if(_label.get_line_count() > n):
		ret = true
	_label.set_text(_label.get_text().left(_label.get_text().length()-word.length())) #omg
	return ret

func _skip_word():
	var ot = _buffer[0]["buff_text"]

	# which comes first, a space or a new line (else, till the end)
	var f_space = ot.findn(" ",1)
	if f_space == -1:
		f_space = ot.length()
	var f_newline = ot.findn("\n",1)
	if f_newline == -1:
		f_newline = ot.length()
	var len = min(f_space, f_newline)

	if(_has_to_skip_word(ot.substr(0,len))):

		if(_buffer[0]["buff_text"][0] == " "):

			_buffer[0]["buff_text"][0] = "\n"
		else:
			_buffer[0]["buff_text"] = _buffer[0]["buff_text"].insert(0,"\n")

# Add text to the label
func _label_print(t):
	var n = _label.get_line_count()
	_label.set_text(_label.get_text() + t)

	printt("TEST", _label.get_line_count(), _label.get_lines_skipped(), _label.get_line_height(), _max_lines)

	# If number of lines increased
	if(_label.get_line_count() > n):

		# If it exceeds _max_lines
		if(_label.get_line_count()-_label.get_lines_skipped() > _max_lines):
			# Reset maximum lines break
			if(_max_lines_reached):
				_max_lines_reached = false

			_label.set_lines_skipped(_label.get_lines_skipped()+1)

		# Add a line breaker, so the engine will be able to get each line
		if (t != "\n" and n > 0):
			_label.set_text(_label.get_text().insert( _label.get_text().length()-1,"\n"))

	return t
