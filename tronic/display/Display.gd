tool
extends "../tronic.gd"

onready var text = get_node("Text/Viewport/TextInterface")

func calculate():
	var data_a = get_node("DataIn")

	if(data_a == null):
		data_a = ""
	else:
		data_a = data_a.get_data()

	var out = str(data_a)

	text.add_text(out)