extends Label

const MAX_LINES = 17
const MAX_LINE_LENGTH = 30
const MAX_LINES_BUFFER = 300

var _max_lines #how many lines we should have at most, depending on size of our control



func add_text(t):
	var out = _split_string(str(t))
	#var out = str(t)

	#printt(out, get_lines_skipped(), get_line_height(), _max_lines, get_visible_line_count(), get_line_count())
	#printt(get_line_count(), get_lines_skipped(), get_visible_line_count())

	set_text(get_text() + out + "\n")

	var amt_lines = get_line_count() - get_visible_line_count()

	#if(amt_lines >= get_lines_skipped()):
	set_lines_skipped(amt_lines-1)
	_clear_skipped_lines()

#===========
# PRIVATE
#===========

# setup
func _ready():
	var height = get_line_height() + get_constant("line_spacing", "Label")

	_max_lines = floor(get_size().y/height)
	#print(_max_lines)

# to be called when the amount of text goes beyond the max buffer to reduce memory over long times?
func _clear_skipped_lines():
	var i = 0
	var n = 0
	while i < get_lines_skipped():
		n = get_text().findn("\n", n)+1
		i+=1
	set_text(get_text().right(n))
	set_lines_skipped(0)

# splits string based on max line length
func _split_string(s):
	if(s.length() <= MAX_LINE_LENGTH):
		return s

	var temp = s
	var out = ""

	while(temp.length() > 0):
		var len = 0
		var add_line = false

		if(temp.length() >= MAX_LINE_LENGTH):
			len = MAX_LINE_LENGTH
			add_line = true
		else:
			len = temp.length()

		out = out + temp.left(len)
		if(add_line):
			out = out + "\n"
		temp = temp.right(len)

	return out