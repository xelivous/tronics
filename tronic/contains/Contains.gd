tool
extends "../tronic.gd"

func calculate():
	var data_a = get_node("DataInA")
	var data_b = get_node("DataInB")
	var data_out = get_node("DataOut")

	if(data_a == null):
		data_a = ""
	else:
		data_a = data_a.get_data()
	if(data_b == null):
		data_b = ""
	else:
		data_b = data_b.get_data()

	var out = bool(str(data_a).find(str(data_b)))

	if(data_out != null):
		data_out.store_data(out)