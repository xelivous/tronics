
extends Resource

export(String) var display_name = "Tronic"
export(String, MULTILINE) var tooltip
export(Texture) var icon
export(Rect2) var icon_region
export(Color) var custom_bg_color
export(bool) var disabled = false

export(PackedScene) var actual_tronic