tool
extends KinematicBody

signal flow_in(tronic_info)
signal flow_out(tronic_info)
signal activated(event_info_object)

var FLOW = preload("../info/flow.tscn")

#override with tronic's main function
func calculate():
	pass

# i'm da best codar
func get_nibs():
	var ret = []

	for x in get_children():
		if(x extends preload("../nib/nib.gd")):
			ret.append(x)

	return ret

# finds the next tronic connected to flow out
func next_tronic():
	var next = get_node("FlowOut").fetch()
	if(next):
		return next.get_parent()
	else:
		return null

func create_flow():
	add_child(FLOW.instance())

func _input_event( camera, event, click_pos, click_normal, shape_idx ):
	if(event.is_action_pressed("activate_tronic")):
		emit_signal("activated", {"camera":camera, "event": event, "click_pos": click_pos, "click_normal": click_normal, "shape_idx": shape_idx})