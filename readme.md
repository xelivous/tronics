currently pretty broken. try going into the addon folder and loading up `tronicTest.tscn` to see what the basic functionality is about.

broken shit i'm aware of
-----------

* if you enable the plugin, you'll get a semi-broken dock that doesn't nothing really in the editor
* if you click on a tronic node in the editor, you'll get a popup editor to assign connections, but it doesn't work
* no easy way to instance tronics you need
* wires don't look correct when you're trying to connect them to something

installation
------------
go to `{folder that contains engine.cfg}/addons/`, and pull this repository into a folder inside of that folder


setup
-----
requires the actions "nib_connect" (preferrably left mouse button) and "nib_disconnect" (preferrably left_ctrl) to be set in your input map in your project settings.