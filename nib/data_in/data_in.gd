tool
extends "../nib.gd"

func can_connect(dest_nib):
	if(dest_nib.nib_type == NIB_TYPE_DATA):
		return true
	else:
		return false

# fetch what this is connected to (should be a data nib), then get the data from it
func get_data():
	var datanode = fetch()

	if(datanode != null):
		return datanode.get_data()
	else:
		return null

# cross-compat because aion is weird and used for multiple purposes
# fetch what this is connected to (should be a data nib), then store the data to it
func store_data(data):
	var datanode = fetch()

	if(datanode != null):
		datanode.store_data(data)