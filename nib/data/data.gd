tool
extends "../nib.gd"

func can_connect(dest_nib):
	if(dest_nib.nib_type == NIB_TYPE_DATA_IN || dest_nib.nib_type == NIB_TYPE_DATA_OUT):
		return true
	else:
		return false

func get_data():
	return get_parent().get_data()

func store_data(data):
	get_parent().store_data(data)