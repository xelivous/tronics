tool
extends Area

var WIRE = preload("../info/wire.tscn")

const NIB_TYPE_NULL = 0
const NIB_TYPE_CHAIN_IN = 1
const NIB_TYPE_CHAIN_OUT = 2
const NIB_TYPE_DATA = 3
const NIB_TYPE_DATA_IN = 4
const NIB_TYPE_DATA_OUT = 5
const NIB_TYPE_FLOW_IN = 6
const NIB_TYPE_FLOW_OUT = 7

export(int, "null", "chain in", "chain out", "data", "data in", "data out", "flow in", "flow out") var nib_type

var connected_to = {} # [ { wire: wireinstance, nib: nibinstance, tronic: tronicinstance }, etc ]

var is_connecting = false

export(bool) var allow_multiple_connections = false

export(Color) var wire_color = Color(255,255,255)
export(Color) var wire_active = Color(255,0,255)
#export(bool) var wire_color_override = false

func _input_event(camera, event, click_pos, click_normal, shape_idx):
	if(event.is_action_pressed("nib_connect") and !is_connecting):
		get_tree().set_input_as_handled()
		is_connecting=true
		var disconnect = Input.is_action_pressed("nib_disconnect_modifier")
		if(disconnect):
			for x in connected_to:
				connected_to[x].wire.disconnect()
		else:
			var wire = WIRE.instance()
			#wire.set_owner(self)
			get_tree().get_root().add_child(wire)
			#if(wire_color_override):
			wire.color_active = wire_active
			wire.color_normal = wire_color
			wire.start_connect(self)

# should override
func can_connect(dest_nib):
	return false

# should return the first entry in what it's connected to, or null
func fetch():
	if(connected_to.size() > 0):
		var k = connected_to.keys()
		connected_to[k[0]].wire.activate_wire()
		return connected_to[k[0]].nib
	else:
		return null

func create_wire_obj(wire, other):
	return { "wire": wire, "nib": other, "tronic": other.get_parent()}

# disconnect all wires
func disconnect_all():
	for x in connected_to:
		connected_to[x].wire.disconnect()

func disconnect(wire, other):
	var id = other.get_instance_ID()
	if(connected_to.has(id)):
		connected_to.erase(id)

func terminate_connect():
	is_connecting = false

func finish_connect(wire, other):
	is_connecting = false

	if (!allow_multiple_connections):
		disconnect_all()

	connected_to[other.get_instance_ID()] = create_wire_obj(wire,other)
