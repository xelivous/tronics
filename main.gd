tool
extends EditorPlugin

var tronic_dock
#var main_tronic_view
var tronic_hooker

var object_to_edit

onready var type_tronic = preload("tronic/tronic.gd")

func _enter_tree():

	tronic_dock = preload("Tronics_Dock.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_UL, tronic_dock)
	tronic_dock.config(get_selection(), get_undo_redo())

	tronic_hooker = preload("Tronic_Hooker.tscn").instance()
	tronic_hooker.config()
	tronic_hooker.hide()
	add_control_to_container(CONTAINER_SPATIAL_EDITOR_BOTTOM, tronic_hooker)

	#main_tronic_view = preload("Tronics_View.tscn").instance()
	#main_tronic_view.hide()
	#add_control_to_container(CONTAINER_SPATIAL_EDITOR_MENU, main_tronic_view)
	#get_base_control().add_child(main_tronic_view)

	print("TRONIC ADDON INIT")

func _exit_tree():
	remove_control_from_docks(tronic_dock)

	print("TRONIC ADDON SHUTDOWN")

# i'd love to be able to use this to modify tronics on the fly
func forward_spatial_input_event(camera, event):
	printt(camera, event)
	get_tree().set_input_as_handled()
	pass

func handles(object):
	if(object extends type_tronic):
		return true

func edit(object):
	object_to_edit = object

#func get_name():
#	return "Tronics"

#func has_main_screen():
#	return true

func make_visible(visible):
	if(visible):
		if(object_to_edit extends type_tronic):
			tronic_hooker.populate(object_to_edit)
			tronic_hooker.show()
	else:
		if(object_to_edit extends type_tronic):
			tronic_hooker.hide()