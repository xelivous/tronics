tool
extends Node

# holds information about what tronics it's been through
# created by a "trigger" type tronic, and sent between tronics to handle the calculation

# holds references of types of tronics with amount of times it's been to them
var history = {}
const MAX_TRONIC_ENTER = 10

var next_tronic

func _ready():
	if(get_history_count(get_parent()) > MAX_TRONIC_ENTER):
		printt(self, "TOO MANY TIMES INTO TRONIC, DESTROYING FLOW")
		queue_free()
	else:
		if(get_parent().has_method("calculate")):
			add_history(get_parent())
			get_parent().calculate()

		var next = get_parent().next_tronic()
		if(next):
			next_tronic = next
			start_timer()

func start_timer():
	get_node("Timer").start()

func get_history_count(obj):
	var id = obj.get_instance_ID()
	if(history.has(id)):
		return history[id]
	else:
		return 0

func add_history(obj):
	var id = obj.get_instance_ID()

	if(!history.has(id)):
		history[id] = 0

	history[id] += 1

# we should add ourselves to the next now
func _on_Timer_timeout():
	get_parent().remove_child(self)
	next_tronic.call_deferred("add_child", self)
