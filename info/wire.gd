extends ImmediateGeometry

var obj1
var obj2

var NIB_TYPE = preload("../nib/nib.gd")

var my_curve

#vector3 of where we're going to click to connect
var ray_from
var ray_to

var color_normal = Color(255,255,255) setget updatenormal
var color_active = Color(255,0,255)

func _ready():
	set_material_override(get_material_override().duplicate())
	#mat.set_point_size(5)
	#mat.set_line_width(5)
	#mat.set_flag(FixedMaterial.FLAG_USE_POINT_SIZE, true)
	#mat.set_
	#set_material_override(mat)

	my_curve = Curve3D.new()
	my_curve.add_point(Vector3(0,0,0))
	my_curve.add_point(Vector3(0,0,0))
	update_geometry()

func updatenormal(col):
	color_normal = col
	update_material()

func update_material():
	var mat = get_material_override()
	mat.set_parameter(FixedMaterial.PARAM_DIFFUSE, color_normal)
	set_material_override(mat)

func update_geometry():
	var geom = my_curve.tesselate()

	clear()
	begin(Mesh.PRIMITIVE_LINE_STRIP)
	for v in geom:
		add_vertex(v)
	end()

func activate_wire(active=true):
	var mat = get_material_override()
	var color = color_active
	if(!active):
		color = color_normal
	mat.set_parameter(FixedMaterial.PARAM_DIFFUSE, color)

	get_node("Timer").start()

# obj = what is initiating the connect
# always set it to obj1, and disconnect the other point if it exists
func start_connect(obj):
	set_point(obj)
	reset_point(true)

	set_process_unhandled_input(true)

func _unhandled_input(event):
	if(event.type == InputEvent.MOUSE_MOTION):
		var cam = get_viewport().get_camera()
		ray_from = cam.project_ray_origin(event.pos)
		ray_to = ray_from + cam.project_ray_normal(event.pos) * cam.get_zfar()
		my_curve.set_point_pos(1, ray_to)
		update_geometry()

	if(event.is_action_pressed("nib_connect")):
		get_tree().set_input_as_handled()
		set_process_unhandled_input(false)
		set_fixed_process(true)

func _fixed_process(delta):
	var space_state = get_world().get_direct_space_state()

	var objs = space_state.intersect_ray(ray_from, ray_to, [], 2147483647, PhysicsDirectSpaceState.TYPE_MASK_AREA)

	if(objs.size() > 0 and objs.collider extends NIB_TYPE and obj1.can_connect(objs.collider)):
		set_point(objs.collider, true)
		if(obj1.has_method("finish_connect")):
			obj1.finish_connect(self, obj2)
		if(obj2.has_method("finish_connect")):
			obj2.finish_connect(self, obj1)
	else:
		obj1.terminate_connect()
		queue_free()

	set_fixed_process(false)

func disconnect():
	if(obj1.has_method("disconnect")):
		obj1.disconnect(self, obj2)
	if(obj2.has_method("disconnect")):
		obj2.disconnect(self, obj1)

	queue_free()

func reset_point(end=false):
	if(end):
		my_curve.remove_point(1)
		my_curve.add_point(Vector3(0,0,0))
	else:
		my_curve.remove_point(0)
		my_curve.add_point(Vector(0,0,0),Vector3(0,0,0),Vector3(0,0,0), 0)
	update_geometry()

func set_point(obj, end=false):
	if(!end):
		obj1 = obj
		my_curve.set_point_pos(0, obj.get_global_transform().origin)
	else:
		obj2 = obj
		my_curve.set_point_pos(1, obj.get_global_transform().origin)
	update_geometry()

# deactivates wire after it fetches
func _on_Timer_timeout():
	activate_wire(false)
