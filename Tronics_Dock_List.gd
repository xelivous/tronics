tool
extends ItemList

# ideally i'd love to dynamically add these
# but LOL good luck finding the addon folder
var tronics = [
	preload("tronic/and/list_and.tres"),
	preload("tronic/plus/list_plus.tres"),
]

func _ready():
	prepare_list()
	add_tronics()
	finalize_list()

func prepare_list():
	clear()
	set_select_mode(SELECT_SINGLE)
	set_icon_mode(ICON_MODE_LEFT)
	set_icon_scale(2.0)

func add_tronics():
	var id = 0 #because there's no easy way to get what index you're on lmao

	for tronic in tronics:
		add_item(tronic.display_name, tronic.icon)

		if(tronic.icon_region != null):
			set_item_icon_region(id, tronic.icon_region)

		set_item_tooltip(id, tronic.tooltip)
		set_item_metadata(id, tronic.actual_tronic)

		if(tronic.custom_bg_color != null):
			set_item_custom_bg_color(id, tronic.custom_bg_color)

		set_item_disabled(id, tronic.disabled)

		id += 1 #make sure to increment after using it

func finalize_list():
	sort_items_by_text()

# doubleclicked
func _on_ItemList_item_activated( index ):
	printt("ACTIVATED", get_item_text(index))
	var obj = get_item_metadata(index).instance()
	add_tronic_to_scene(obj)

# singleclicked
func _on_ItemList_item_selected( index ):
	printt("SELECTED", get_item_text(index))

func add_tronic_to_scene(obj):
	var undo_redo = get_node("..").undo_redo

	var node = get_node("..").selector.get_selected_nodes()[0]
	var edited_scene = get_tree().get_edited_scene_root()

	if not node and edited_scene:
		printt("Yo something went wrong adding tronic to scene", node, edited_scene)
		return

	var mesh_instance = obj

	undo_redo.create_action("Create " + mesh_instance.get_name())

	if edited_scene:
		undo_redo.add_do_method(node, "add_child", mesh_instance)
		undo_redo.add_do_method(mesh_instance, "set_owner", edited_scene)
		undo_redo.add_do_reference(mesh_instance)

		undo_redo.add_undo_method(node, "remove_child", mesh_instance)

	else:
		var editor = get_tree().get_root().get_node("EditorNode")

		undo_redo.add_do_method(editor, "set_edited_scene", mesh_instance)
		undo_redo.add_do_reference(mesh_instance)

		undo_redo.add_undo_method(editor, "set_edited_scene", Object(null))

	undo_redo.commit_action()
